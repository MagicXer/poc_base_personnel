import { BasePersonnelFrontPage } from './app.po';

describe('base-personnel-front App', function() {
  let page: BasePersonnelFrontPage;

  beforeEach(() => {
    page = new BasePersonnelFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
