import { Component, OnInit } from '@angular/core';
import { Http, RequestOptionsArgs, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs';

import { Personnel } from './personnel';
import { PersonnelApi } from './personnel.api';

@Component({
  selector: 'app-personnel',
  providers: [PersonnelApi],
  templateUrl: './personnel.component.html',
  styleUrls: ['./personnel.component.css']
})
export class PersonnelComponent implements OnInit {

  public values: Personnel[];

  constructor(private _dataService: PersonnelApi) { }

  ngOnInit() {
    this._dataService.Personnel_GetAll().then(
      (data: Personnel[]) => {
        this.values = data
      });
  }

}
