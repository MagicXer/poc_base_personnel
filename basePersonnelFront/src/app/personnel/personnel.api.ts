import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';

import { Personnel } from './personnel';
import {globalVariables} from '../globals';
import { globalAgent } from 'http';

@Injectable()
export class PersonnelApi {
    constructor(private http: Http) {}

    public Personnel_GetAll (extraHttpRequestParams?: any ) : Promise<Array<Personnel>> {
        return this.Personnel_GetAllAsObservable( extraHttpRequestParams)
                .toPromise()
                .then(response => { 
                    try { return response.json(); } catch (e) {
                        // suppress
                    }
                });
    }

    public Personnel_GetAllAsObservable (extraHttpRequestParams?: any ) : Observable<Response> {
        let path = globalVariables.api_url_personnel + '/api/personnel';

        let queryParameters: URLSearchParams = new URLSearchParams();
        let headerParams: Headers = new Headers();

        headerParams.append('Content-Type', 'application/json');
        headerParams.append('Access-Control-Allow-Origin', '*');

        if (extraHttpRequestParams) {
            for (let k in extraHttpRequestParams) {
                if (extraHttpRequestParams.hasOwnProperty(k)) {
                    headerParams.append(k, extraHttpRequestParams[k]);
                }
            }
        }

        let httpRequestParams: RequestOptionsArgs = {
            method: 'GET',
            
            search: queryParameters,
            headers: headerParams
        };

        return this.http.request(path, httpRequestParams);
    }
}
