import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { FormationComponent } from './formation/formation.component';
import { PersonnelComponent } from './personnel/personnel.component';

const appRoutes: Routes = [
  { path: 'formation', component: FormationComponent },
  { path: 'personnel', component: PersonnelComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    FormationComponent,
    PersonnelComponent,
    PersonnelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
