import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';

import { Formation } from './formation';
import {globalVariables} from '../globals';

@Injectable()
export class FormationApi {

    constructor(private http: Http) {}

    public Formation_GetAll (extraHttpRequestParams?: any ) : Promise<Array<Formation>> {
        return this.Formation_GetAllAsObservable( extraHttpRequestParams)
                .toPromise()
                .then(response => { 
                    try { 
                        console.log(response.json());
                        return response.json(); } 
                    catch (e) {
                        console.log(e);
                    }
                });
    }

    public Formation_GetAllAsObservable (extraHttpRequestParams?: any ) : Observable<Response> {
        let path = globalVariables.api_url_formation + '/api/formation';

        let queryParameters: URLSearchParams = new URLSearchParams();
        let headerParams: Headers = new Headers();

        headerParams.append('Content-Type', 'application/json');
        headerParams.append('Access-Control-Allow-Origin', '*');

        if (extraHttpRequestParams) {
            for (let k in extraHttpRequestParams) {
                if (extraHttpRequestParams.hasOwnProperty(k)) {
                    headerParams.append(k, extraHttpRequestParams[k]);
                }
            }
        }

        let httpRequestParams: RequestOptionsArgs = {
            method: 'GET',
            
            search: queryParameters,
            headers: headerParams
        };

        return this.http.request(path, httpRequestParams);
    }
}
