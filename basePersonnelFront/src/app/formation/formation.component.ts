import { Component, OnInit } from '@angular/core';
import { Http, RequestOptionsArgs, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs';

import {Formation} from './formation';
import {FormationApi} from './formation.api';

@Component({
    selector: 'app-formation',
    providers: [FormationApi],
    templateUrl: './formation.component.html',
    styleUrls: ['./formation.component.css']
})

export class FormationComponent implements OnInit {
    
    public values: Formation[];

    constructor(private _dataService: FormationApi) { 
    }

    ngOnInit() {
        this._dataService.Formation_GetAll().then(
            (data: Formation[]) => {
                console.log(data);
                this.values = data
            });
    }

}
