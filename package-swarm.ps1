# Build all images
cd .\basePersonnelFront
docker build -t base-front .

cd ..\basePersonnelBack\backFormationApp
docker build -t back-formation .

cd ..\basePersonnelBack\backPersonnelApp
docker build -t back-personnel .

# Initiaze Docker Manager
docker swarm init

# If you are more 1 worker
# docker swarm join --token [TOKEN SWARM INIT] [IP:PORT] (ex : SWMTKN-1-4yyiimv1awpl1qdhsclr03vyc1haagmz7estl6t6vwoqzie6yn-2ve0ponwhqqew0khku3hm5kih 10.0.0.4:2377)

#List all nodes
docker node ls

#Add label for the host
docker node update --label-add os=windows personnelvm

# Create network 
docker network create -d overlay personnel_net --subnet=192.168.0.0/16 --gateway=192.168.0.100 --ip-range=192.168.1.0/24

# Create service BACK FORMATION with last image back-formation (start container)
docker service create --hostname back-formation-host --name back-formation-service --endpoint-mode dnsrr --publish mode=host,target=8000,published=8000 --network personnel_net --constraint 'node.labels.os==windows' back-formation

# Create service BACK PERSONNEL with last image back-personnel (start container)
docker service create --hostname back-personnel-host --name back-personnel-service --endpoint-mode dnsrr --publish mode=host,target=8001,published=8001 --network personnel_net --constraint 'node.labels.os==windows' back-personnel

# Create service for FRONT application with last image base-fron (start container)
docker service create --name front-service --endpoint-mode dnsrr --publish mode=host,target=4200,published=8080 --network personnel_net --constraint 'node.labels.os==windows' base-front