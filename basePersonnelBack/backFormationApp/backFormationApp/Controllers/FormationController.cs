﻿using backFormationApp.Models;
using backFormationApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace backFormationApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FormationController : ApiController
    {
        private readonly FormationRepository _repository = new FormationRepository();

        [HttpGet]
        public Formation[] GetAll()
        {
            return _repository.GetAll();
        }
    }
}