﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backFormationApp.Models
{
    public class Formation
    {
        public string Name { get; set; }
        public int Duree { get; set; }
        public string Lieu { get; set; }
        public float Cout { get; set; }
    }
}