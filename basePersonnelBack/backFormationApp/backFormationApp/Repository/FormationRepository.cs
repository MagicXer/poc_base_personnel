﻿using backFormationApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backFormationApp.Repository
{
    public class FormationRepository
    {
        public Formation[] GetAll()
        {
            return new Formation[]
            {
                new Formation{Cout = 1500.50F, Duree = 12, Lieu = "Ibiza", Name = "Accueil" },
                new Formation{Cout = 2300.50F, Duree = 12, Lieu = "Pékin", Name = "Formation L2"  },
                new Formation{Cout = 3000F, Duree = 12, Lieu = "New-York", Name = "Formation L1"  },
                new Formation{Cout = 500.50F, Duree = 12, Lieu = "Clermont-Ferrand", Name = "Accueil" }
            };
        }
    }
}