﻿using backPersonnelApp.Models;
using backPersonnelApp.Repository;
using System.Web.Http;
using System.Web.Http.Cors;

namespace backPersonnelApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PersonnelController : ApiController
    {
        private readonly PersonnelRepository _repository = new PersonnelRepository();

        [HttpGet]
        public Personnel[] GetAll()
        {
            return _repository.GetAll();
        }
    }
}