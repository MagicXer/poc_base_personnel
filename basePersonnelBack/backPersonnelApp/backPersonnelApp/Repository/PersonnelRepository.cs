﻿using backPersonnelApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backPersonnelApp.Repository
{
    public class PersonnelRepository
    {
        public Personnel[] GetAll()
        {
            return new Personnel[]
            {
                new Personnel{Age = 28, Name = "Fabien", LastName = "Vergnes", Poste = "DBA" },
                new Personnel{Age = 24, Name = "Sylvain", LastName = "Le Gouellec", Poste = "Développeur" },
                new Personnel{Age = 30, Name = "Jérémy", LastName = "Brigaud", Poste = "Chef de projet" }
            };
        }
    }
}