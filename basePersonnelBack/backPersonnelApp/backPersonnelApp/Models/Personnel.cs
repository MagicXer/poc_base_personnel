﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backPersonnelApp.Models
{
    public class Personnel
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Poste { get; set; }
    }
}